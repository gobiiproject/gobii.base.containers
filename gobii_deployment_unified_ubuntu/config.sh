#!/bin/bash
#Gobii web setup
sed -i "s|<GlobalNamingResources>|<GlobalNamingResources>\n<Environment name=\"gobiipropsloc\" value=\"$mountpoint\/$gobiiroot\/$gobiiconfigpath\" type=\"java.lang.String\" override=\"false\"\/>|" /usr/local/tomcat/conf/server.xml
sed -i "s/<\/tomcat-users>/<user username=\"$gobii_tomcat_user\" password=\"$gobii_tomcat_passwd\" roles=\"manager-gui, manager-status, manager-script, manager-jmx\"\/>\n<\/tomcat-users>/" /usr/local/tomcat/conf/tomcat-users.xml
chown -R $gobiiuser:$gobiigroup /usr/local/tomcat
su $gobiiuser -c "/usr/local/tomcat/bin/startup.sh"

#Gobii compute setup
#no action required

#Gobii db setup
sed -i "s/local   all             all                                     peer/local   all             all                                     $postgres_local_auth_method/" /etc/postgresql/9.5/main/pg_hba.conf
sed -i "s/host    all             all             127\.0\.0\.1\/32            md5/host    all             all             0\.0\.0\.0\/0            	$postgres_host_auth_method/" /etc/postgresql/9.5/main/pg_hba.conf
sed -i "s/\#listen_addresses = 'localhost'/listen_addresses = '$postgres_listen_address'/" /etc/postgresql/9.5/main/postgresql.conf
service postgresql start

service ssh restart
su $gobiiuser -c "/bin/bash"