FROM armahke/gobii_ubuntu:latest
#Gobii web setup
RUN apt-get update -y
RUN apt-get install python-software-properties software-properties-common -y
RUN add-apt-repository ppa:webupd8team/java -y
RUN apt-get update -y
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get -y install oracle-java8-installer
RUN apt-get -y install oracle-java8-set-default
RUN wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.73/bin/apache-tomcat-7.0.73.tar.gz
RUN tar -xzf apache-tomcat-7.0.73.tar.gz
RUN mv apache-tomcat-7.0.73 /usr/local/tomcat
RUN sed -i 's/#!\/bin\/sh/#!\/bin\/sh\numask 002/' /usr/local/tomcat/bin/startup.sh
RUN sed -i 's/#!\/bin\/sh/#!\/bin\/sh\nJRE_HOME\=\/usr\/lib\/jvm\/java-8-oracle/' /usr/local/tomcat/bin/catalina.sh
RUN sed -i 's/<Context>/<Context>\n<ResourceLink name="gobiipropsloc" global="gobiipropsloc" type="java.lang.String"\/>/' /usr/local/tomcat/conf/context.xml

#Gobii compute setup
RUN apt-get update -y
RUN apt-get install python-software-properties software-properties-common build-essential zlib1g-dev -y libbz2-dev liblzma-dev
RUN add-apt-repository ppa:webupd8team/java -y
RUN apt-get update -y
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get -y install oracle-java8-installer
RUN apt-get -y install oracle-java8-set-default
RUN apt-get -y install realpath
RUN apt-get -y install python2.7-dev
RUN apt-get -y install python-psycopg2
RUN apt-get -y install curl
RUN apt-get install -y python-pip
RUN pip install --upgrade pip
RUN pip install Numpy
RUN pip install pandas
USER gadm
RUN curl -L http://xrl.us/installperlnix | bash
RUN /home/gadm/perl5/perlbrew/bin/cpanm Text::CSV
RUN /home/gadm/perl5/perlbrew/bin/cpanm Canary::Stability
RUN /home/gadm/perl5/perlbrew/bin/cpanm JSON::XS
RUN /home/gadm/perl5/perlbrew/bin/cpanm Switch
RUN cd /home/gadm; wget https://github.com/samtools/htslib/releases/download/1.4/htslib-1.4.tar.bz2
RUN cd /home/gadm; wget https://github.com/samtools/bcftools/releases/download/1.4/bcftools-1.4.tar.bz2
RUN cd /home/gadm; tar -xjf htslib-1.4.tar.bz2
RUN cd /home/gadm; tar -xjf bcftools-1.4.tar.bz2
RUN cd /home/gadm; mv htslib-1.4 htslib
RUN cd /home/gadm; mv bcftools-1.4 bcftools
RUN cd /home/gadm/bcftools; make
RUN cd /home/gadm; wget https://s3.us-east-2.amazonaws.com/gobii-archives/vcftools_0.1.13.tar.gz
RUN cd /home/gadm; tar -xzf vcftools_0.1.13.tar.gz
RUN cd /home/gadm/vcftools_0.1.13; make
RUN echo export PERL5LIB=/home/gadm/vcftools_0.1.13/perl >> /home/gadm/.bashrc
RUN echo export PATH=$PATH:/home/gadm/vcftools_0.1.13/bin/ >> /home/gadm/.bashrc
RUN ["/bin/bash", "-c", "source /home/gadm/.bashrc"]
USER root

#Gobii db setup
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main 9.5" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update -y
RUN apt-get install -y postgresql-9.5 postgresql-contrib-9.5
EXPOSE 5432

COPY config.sh /root
RUN chmod 755 /root/config.sh
ENTRYPOINT ["/root/config.sh"]