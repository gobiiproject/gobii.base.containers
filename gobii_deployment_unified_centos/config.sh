#!/bin/bash
#Gobii web setup
sed -i "s|<GlobalNamingResources>|<GlobalNamingResources>\n<Environment name=\"gobiipropsloc\" value=\"$mountpoint\/$gobiiroot\/$gobiiconfigpath\" type=\"java.lang.String\" override=\"false\"\/>|" /usr/local/tomcat/conf/server.xml
sed -i "s/<\/tomcat-users>/<user username=\"$gobii_tomcat_user\" password=\"$gobii_tomcat_passwd\" roles=\"manager-gui, manager-status, manager-script, manager-jmx\"\/>\n<\/tomcat-users>/" /usr/local/tomcat/conf/tomcat-users.xml
chown -R $gobiiuser:$gobiigroup /usr/local/tomcat
su $gobiiuser -c "/usr/local/tomcat/bin/startup.sh"

#Gobii compute setup
#no action required

#Gobii db setup
sed -i "s/local   all             all                                     peer/local   all             all                                     $postgres_local_auth_method/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i "s/host    all             all             127\.0\.0\.1\/32            ident/host    all             all             0\.0\.0\.0\/0            $postgres_host_auth_method/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i "s/\#listen_addresses = 'localhost'/listen_addresses = '$postgres_listen_address'/" /var/lib/pgsql/9.5/data/postgresql.conf
su -l postgres -c "nohup /usr/pgsql-9.5/bin/pg_ctl -D /var/lib/pgsql/9.5/data start &"

nohup /usr/sbin/sshd -D &
su $gobiiuser -c "/bin/bash"