#!/bin/bash
sed -i "s/local   all             all                                     peer/local   all             all                                     $postgres_local_auth_method/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i "s/host    all             all             127\.0\.0\.1\/32            ident/host    all             all             0\.0\.0\.0\/0            $postgres_host_auth_method/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i "s/\#listen_addresses = 'localhost'/listen_addresses = '$postgres_listen_address'/" /var/lib/pgsql/9.5/data/postgresql.conf
su -l postgres -c "nohup /usr/pgsql-9.5/bin/pg_ctl -D /var/lib/pgsql/9.5/data start &"
nohup /usr/sbin/sshd -D &
/bin/bash