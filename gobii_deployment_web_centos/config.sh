#!/bin/bash
sed -i "s|<GlobalNamingResources>|<GlobalNamingResources>\n<Environment name=\"gobiipropsloc\" value=\"$mountpoint\/$gobiiroot\/$gobiiconfigpath\" type=\"java.lang.String\" override=\"false\"\/>|" /usr/local/tomcat/conf/server.xml
sed -i "s/<\/tomcat-users>/<user username=\"$gobii_tomcat_user\" password=\"$gobii_tomcat_passwd\" roles=\"manager-gui, manager-status, manager-script, manager-jmx\"\/>\n<\/tomcat-users>/" /usr/local/tomcat/conf/tomcat-users.xml
chown -R $gobiiuser:$gobiigroup /usr/local/tomcat
su $gobiiuser -c "/usr/local/tomcat/bin/startup.sh"
nohup /usr/sbin/sshd -D &
su $gobiiuser -c "/bin/bash"